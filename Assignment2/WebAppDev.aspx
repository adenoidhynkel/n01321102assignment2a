﻿<%@ Page Title="Web Application Development" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebAppDev.aspx.cs" Inherits="Assignment2.WebAppDev" %>
<asp:Content ID="webAppsMain" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1><%: Page.Title %></h1>
    </div>
    <h2>Conditional statements and Loops <span class="glyphicon glyphicon-grain"></span></h2>
    <h3>Conditional statements</h3>
    <p class="whitespace"> A <strong class="text-info">conditional statement</strong> is a programming logic that helps a user filter out or decide on something based on certain conditions.
    <em class="text-info">IF</em>, <em class="text-info">ELSE IF</em>, and <em class="text-info">SWITCH</em> statements fall in this category. 
    It is also known as "selection" in algorithms or when using flowcharts.</p>
    <h3>Loops</h3>
    <p class="whitespace">In algorithms, this is called an "iteration"; a <strong class="text-info">Loop</strong> is a programming logic that will repeat a number of times or until certain conditions are met.
        <em class="text-info">FOR</em>, <em class="text-info">FOR EACH</em>, <em class="text-info">WHILE</em>, and <em class="text-info">DO WHILE</em> are some of the loops used.</p>
    <h3>Why is it Tricky?</h3>
    <p>With all the conditional statements and loops that are available, a new developer would ask, which one should I use in a specific situation? and it gets messy when you try to nest them!</p>
    <h3> IF/ELSE IF VS SWITCH</h3>
        <ul>
            <li>We use the <em class="text-info">IF</em> statement if we want to evaluate a condition if it's true or false i.e. IF this is true then do this ELSE do this.</li>
            <li><em class="text-info">ELSE IF</em> is added or used in the statement if the user wants to evaluate another condition.</li>
            <li>We use the <em class="text-info">SWITCH</em> if we are expecting more than 2 values to evaluate from a variable.</li>
        </ul>
    <h3> FOR/FOREACH VS WHILE/DO WHILE</h3>
        <ul>
            <li>We use the <em class="text-info">FOR</em> loop if we certainly know how many times we have to repeat a block of code.</li>
            <li>We use the <em class="text-info">FOR EACH</em> loop when we want to manipulate or retrieve the values from an array.</li>
            <li>We use the <em class="text-info">WHILE</em> loop when we don't know or sure how many times we have to loop,<br/> which we only rely on a state variable which can be true or false.</li>
            <li>We use the <em class="text-info">DO WHILE</em> loop if we want to run the code block at least once before evaluating it. </li>
        </ul>
</asp:Content>

<asp:Content ID="webAppsCodeSnippet" ContentPlaceHolderID="sectionCode" runat="server">
   <h2>My Code Examples <span class="glyphicon glyphicon-chevron-left"></span><span class="glyphicon glyphicon-chevron-right"></span></h2>
    <ol id="codeExamples" class="webAppList">
        <li><strong class="text-info">IF</strong> Statement
            <p>For example we need to evaluate if the variable value is less than 10.</p>
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
               int number = 11;
               string message = "";
             if(number < 10)
             {
               message = number + " is less than 10";
             }
             else
             {
                message = number + " is greater than 10";  
             }

           </code>
         </div>
           <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#if_sample_print">Show Result</button>
        </li>        
        <li><strong class="text-info">ELSE IF</strong> Statement
            <p>For example we need to set a certain message on each condition it satisfies</p>
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
               int number = 11;
               string message = "";
             if(number < 10)
             {
               message = " Hello";
             }
             else if(number < 20)
             {
                message = "Hello World";  
             }
            else
             {
                message = "Goodbye";
             }

           </code>
         </div>
          <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#else_if_sample_print">Show Result</button>
        </li>        
        <li><strong class="text-info">SWITCH</strong> Statement
            <p>For example we need to create a simple calculator program that gives user choices and will do the task based on the choice given by user</p>
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                int choice = int.Parse(taskChoice.Text);
                switch(choice)
                {
                    case 1: performAddition();
                        break;
                    case 2: performSubtraction();
                        break;
                    case 3: performMultiplication();
                        break;
                    case 4: performDivision();
                        break;                    
                    case 5: exitProgram();
                        break;
                    default: errorMSG();
                }

           </code>
         </div>
        </li>        
        <li><strong class="text-info">FOR</strong> Loop
         <p>For example we need to print Hello World 20 times</p>
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                for(int ctr=0;ctr < 20; ctr++)
                {
                  messageArea += "Hello World < br />";  
                }

           </code>
         </div>
          <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#for_sample_print">Show Result</button>
        </li>        
        <li><strong class="text-info">FOR EACH</strong> Loop
         <p>For example we need to print numbers in an array</p>
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                List<int> myNumbers = new List<int> {1,2,3,4,5};
                foreach(int numbers in myNumbers)
                {
                  messageArea += numbers + ", ";  
                }

           </code>
         </div>
         <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#for_each_sample_print">Show Result</button>
        </li>        
        <li><strong class="text-info">WHILE</strong> Loop
         <p>For example we have a login system the program will continue to run until the user has chosen to exit </p>
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
               Boolean stillRunning = true;
                while(stillRunning == true)
                {
                    int choice = int.Parse(taskChoice.Text);
                    switch(choice)
                    {
                        case 1: performAddition();
                            break;
                        case 2: performSubtraction();
                            break;
                        case 3: performMultiplication();
                            break;
                        case 4: performDivision();
                            break;                    
                        case 5: stillRunning = false; //set to false
                            break;
                        default: errorMSG();
                    }
               }

           </code>
         </div>
        </li>        
        <li><strong class="text-info">DO WHILE</strong> Loop
         <p>same as the previous example</p>
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
               Boolean stillRunning = true;
                do
                {
                    int choice = int.Parse(taskChoice.Text);
                    switch(choice)
                    {
                        case 1: performAddition();
                            break;
                        case 2: performSubtraction();
                            break;
                        case 3: performMultiplication();
                            break;
                        case 4: performDivision();
                            break;                    
                        case 5: stillRunning = false; //set to false
                            break;
                        default: errorMSG();
                    }
               }
               while(stillRunning == true);

           </code>
         </div>
        </li>
    </ol>
                
    
    <!-- MODALS -->
        <div id="if_sample_print" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ifPrintResult">Result</h4>
                     <br>
                    <code class="codespace">
                       int number = 11;
                       string message = "";
                     if(number < 10)
                     {
                       message = number + " is less than 10";
                     }
                     else
                     {
                        message = number + " is greater than 10";  
                     }
                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p class="codespace">
                        11 is greater than 10
                    </p>
                </div>
            </div>
        </div>         
        <div id="else_if_sample_print" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="elseifPrintResult">Result</h4>
                     <br>
                    <code class="codespace">
                           int number = 11;
                           string message = "";
                         if(number < 10)
                         {
                           message = " Hello";
                         }
                         else if(number < 20)
                         {
                            message = "Hello World";  
                         }
                        else
                         {
                            message = "Goodbye";
                         }
                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p class="codespace">
                        Hello
                    </p>
                </div>
            </div>
        </div>         
        <div id="for_sample_print" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="forPrintResult">Result</h4>
                     <br>
                    <code class="codespace">
                        for(int ctr=0;ctr < 20; ctr++)
                        {
                          messageArea += "Hello World< br/ >";  
                        }
                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p class="codespace">
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                        Hello World
                    </p>
                </div>
            </div>
        </div>         
    <div id="for_each_sample_print" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="forEachPrintResult">Result</h4>
                     <br>
                    <code class="codespace">
                        List<int> myNumbers = new List<int> {1,2,3,4,5};
                        foreach(int numbers in myNumbers)
                        {
                          messageArea += numbers + ", ";  
                        }
                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p class="codespace">
                       1, 2, 3, 4, 5
                    </p>
                </div>
            </div>
        </div> 
</asp:Content>

<asp:Content ID="webAppsLectureContent" ContentPlaceHolderID="sectionLecture" runat="server">
   <h2>More Examples <span class="glyphicon glyphicon-chevron-left"></span><span class="glyphicon glyphicon-chevron-right"></span></h2>
        <ol id="moreExamples" class="webAppList">
        <li><strong class="text-info">IF</strong> Statement
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                bool condition = true;

                if (condition)
                {
                    Console.WriteLine("The variable is set to true.");
                }
                else
                {
                    Console.WriteLine("The variable is set to false.");
                }
           </code>
         </div>
           <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#if_sample_check">Show Result</button>
        </li>        
        <li><strong class="text-info">ELSE IF</strong> Statement
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                Console.Write("Enter a character: ");
                char ch = (char)Console.Read();

                if (Char.IsUpper(ch))
                {
                    Console.WriteLine("The character is an uppercase letter.");
                }
                else if (Char.IsLower(ch))
                {
                    Console.WriteLine("The character is a lowercase letter.");
                }
                else if (Char.IsDigit(ch))
                {
                    Console.WriteLine("The character is a number.");
                }
                else
                {
                    Console.WriteLine("The character is not alphanumeric.");
                }
           </code>
         </div>
        </li>        
        <li><strong class="text-info">SWITCH</strong> Statement
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                  switch (caseSwitch)
                  {
                      case 1:
                          Console.WriteLine("Case 1");
                          break;
                      case 2:
                          Console.WriteLine("Case 2");
                          break;
                      default:
                          Console.WriteLine("Default case");
                          break;
                  }
           </code>
         </div>
        </li>        
        <li><strong class="text-info">FOR</strong> Loop
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("Value of i: {0}", i);
                }
           </code>
         </div>
          <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#for_sample_count">Show Result</button>
        </li>        
        <li><strong class="text-info">FOR EACH</strong> Loop
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                var fibNumbers = new List<int> { 0, 1, 1, 2, 3, 5, 8, 13 };
                int count = 0;
                foreach (int element in fibNumbers)
                {
                    count++;
                    Console.WriteLine($"Element #{count}: {element}");
                }
                Console.WriteLine($"Number of elements: {count}");
           </code>
         </div>
          <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#foreach_sample_fibo">Show Result</button>
        </li>        
        <li><strong class="text-info">WHILE</strong> Loop
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                int i = 0;

                while (i < 10)
                {
                    Console.WriteLine("Value of i: {0}", i);

                    i++;
                }
           </code>
         </div>
          <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#while_sample_increment">Show Result</button>
        </li>        
        <li><strong class="text-info">DO WHILE</strong> Loop
          <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                int i = 0;

                do
                {
                    Console.WriteLine("Value of i: {0}", i);
    
                    i++;
    
                    if (i > 5)
                        break;

                } while (true);

           </code>
         </div>
           <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#do_while_sample_break">Show Result</button>
        </li>
    </ol>

            <!-- MODALS -->
        <div id="if_sample_check" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ifResult">Result</h4>
                     <br>
                    <code class="codespace">
                        bool condition = true;

                        if (condition)
                        {
                            Console.WriteLine("The variable is set to true.");
                        }
                        else
                        {
                            Console.WriteLine("The variable is set to false.");
                        }
                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p class="codespace">
                        The variable is set to true.
                    </p>
                </div>
            </div>
        </div>         
        <div id="for_sample_count" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="forCountResult">Result</h4>
                     <br>
                    <code class="codespace">
                        for (int i = 0; i < 10; i++)
                        {
                            Console.WriteLine("Value of i: {0}", i);
                        }
                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p class="codespace">
                        Value of i: 0 
                        Value of i: 1 
                        Value of i: 2 
                        Value of i: 3 
                        Value of i: 4 
                        Value of i: 5 
                        Value of i: 6 
                        Value of i: 7 
                        Value of i: 8 
                        Value of i: 9

                    </p>
                </div>
            </div>
        </div>         
        <div id="foreach_sample_fibo" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="fiboResult">Result</h4>
                     <br>
                    <code class="codespace">
                        var fibNumbers = new List<int> { 0, 1, 1, 2, 3, 5, 8, 13 };
                        int count = 0;
                        foreach (int element in fibNumbers)
                        {
                            count++;
                            Console.WriteLine($"Element #{count}: {element}");
                        }
                        Console.WriteLine($"Number of elements: {count}");
                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p class="codespace">
                        Element #1: 0
                        Element #2: 1
                        Element #3: 1
                        Element #4: 2
                        Element #5: 3
                        Element #6: 5
                        Element #7: 8
                        Element #8: 13
                        Number of elements: 8

                    </p>
                </div>
            </div>
        </div>          
        <div id="while_sample_increment" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="incrementResult">Result</h4>
                     <br>
                    <code class="codespace">
                        int i = 0;

                        while (i < 10)
                        {
                            Console.WriteLine("Value of i: {0}", i);

                            i++;
                        }
                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p class="codespace">
                        Value of i: 0 
                        Value of i: 1 
                        Value of i: 2 
                        Value of i: 3 
                        Value of i: 4 
                        Value of i: 5 
                        Value of i: 6 
                        Value of i: 7 
                        Value of i: 8 
                        Value of i: 9

                    </p>
                </div>
            </div>
        </div>         
        <div id="do_while_sample_break" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="sampleBreakResult">Result</h4>
                     <br>
                    <code class="codespace">
                        int i = 0;

                        do
                        {
                            Console.WriteLine("Value of i: {0}", i);
    
                            i++;
    
                            if (i > 5)
                                break;

                        } while (true);
                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p class="codespace">
                        Value of i: 0 
                        Value of i: 1 
                        Value of i: 2 
                        Value of i: 3 
                        Value of i: 4 
                        Value of i: 5

                    </p>
                </div>
            </div>
        </div> 
</asp:Content>

<asp:Content ID="webAppsLinks" ContentPlaceHolderID="sectionLinks" runat="server">
    <h2>Useful Links <span class="glyphicon glyphicon-link"></span></h2>
     <ul>
        <li><a href="https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/statement-keywords">Microsoft Documentation</a></li>
        <li><a href="http://www.tutorialsteacher.com/csharp/csharp-do-while-loop">Tutorials Teacher</a></li>
    </ul>
</asp:Content>