﻿<%@ Page Title="Web Programming" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebProgramming.aspx.cs" Inherits="Assignment2.WebProgramming" %>
<asp:Content ID="webProgMain" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1><%: Page.Title %></h1>
    </div>
    <h2>Arrays and Objects <span class="glyphicon glyphicon-th-list"></span></h2>
    <h3>Arrays</h3>
    <p>An array contains a list of values contained within a variable which can be accessed through its index. Data types can be string, number, bool or a combination of each data type.</p>    
    <h3>Objects</h3>
    <p>An object is somewhat similar to an array as but it has a list of values called properties and functions called methods contained within a variable. </p>
    <h3>Why is it Tricky?</h3>
    <p>Actually, I don't find it tricky but it's an interesting feature, as it offers flexibility depending of what we need.</p>    
    <h3>Use Cases</h3>
    <p>We can use arrays if we only want to store and retrieve values. We use objects if we want to create multiple instances of a variable that have methods that can dynamically update its contents i.e. a player object</p>
</asp:Content>

<asp:Content ID="webProgCodeSnippet" ContentPlaceHolderID="sectionCode" runat="server">
    <h2>My Code Examples <span class="glyphicon glyphicon-chevron-left"></span><span class="glyphicon glyphicon-chevron-right"></span></h2>
    <ol id="codeExamples" class="webAppList">
        <li><strong class="text-info">Arrays</strong>
           <p>For Example, I want to create an array of names, and I want to access the name "Jojo" and print it on the console</p>
           <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                var myNameArray = ["Joseph", "Jonathan", "Dio" ,"Jojo", "Will", "Caesar"];
                console.log("It was you! " + myNameArray[2] + "!");

           </code>
         </div>
          <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#array_sample_names">Show Result</button>
        </li>        
        <li><strong class="text-info">Objects</strong>
           <p>For Example, I want to create a character object, and I want to access the name, job, and base level and print it on the console</p>
           <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                var myRpgCharacter = {
                name : "Clayton Bigsby",
                job  : "Wizard",
                baseLevel: 99,
                jobLevel: 35,
                hitPoints: 999,
                items: ["books", "potions", "white robe" , "white pointy hat"],
                receiveDamage: function(){//some code},
                healDamage: function(){//some code}
                }; 

               console.log("Howdy, " + myRpgCharacter.name + "! you are currently a level " + myRpgCharacter.baseLevel + " " + myRpgCharacter.job);

           </code>
         </div>
           <br/>
           <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#object_sample_characters">Show Result</button>
        </li>
    </ol>

        <!-- MODALS -->
        <div id="array_sample_names" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="arrNamesResult">Result</h4>
                     <br>
                    <code class="codespace">
                       var myNameArray = ["Joseph", "Jonathan", "Dio" ,"Jojo", "Will", "Caesar"];
                       console.log("It was you! " + myNameArray[2] + "!");

                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p>It was you! Dio!</p>
                </div>
            </div>
        </div>         
        <div id="object_sample_characters" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="objCharResult">Result</h4>
                     <br>
                       <code class="codespace">
                            var myRpgCharacter = {
                            name : "Clayton Bigsby",
                            job  : "Wizard",
                            baseLevel: 99,
                            jobLevel: 35,
                            hitPoints: 999,
                            items: ["books", "potions", "white robe" , "white pointy hat"],
                            receiveDamage: function(){//some code},
                            healDamage: function(){//some code}
                            }; 

                           console.log("Howdy, " + myRpgCharacter.name + "! you are currently a level " + myRpgCharacter.baseLevel + " " + myRpgCharacter.job);

                       </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <p>Howdy, Clayton Bigsby! you are currently a level 99 Wizard</p>
                </div>
            </div>
        </div> 
</asp:Content>

<asp:Content ID="webProgLectureContent" ContentPlaceHolderID="sectionLecture" runat="server">
    <h2>More Examples <span class="glyphicon glyphicon-chevron-left"></span><span class="glyphicon glyphicon-chevron-right"></span></h2>
        <ol id="moreExamples" class="webAppList">
        <li><strong class="text-info">Arrays</strong>
           <p>Another way of declaring an Array</p>
           <div class="col-md-12 col-xs-12 well well-sm">
           <code>
                var cars = new Array("Saab", "Volvo", "BMW");
                document.getElementById("demo").innerHTML = cars;

           </code>
         </div>
           <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#array_sample_cars">Show Result</button>
        </li>        
        <li><strong class="text-info">Objects</strong>
           <p>Creating a person object and accessing its properties</p>
           <div class="col-md-12 col-xs-12 well well-sm">
           <code>
               // Create an object:
                var person = {
                    firstName : "John",
                    lastName  : "Doe",
                    age       : 50,
                    eyeColor  : "blue"
                };

                // Display some data from the object:
                document.getElementById("demo").innerHTML =
                person.firstName + " is " + person.age + " years old.";

           </code>
         </div>
          <br/>
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#object_sample_person">Show Result</button>
        </li>
    </ol>
            <!-- MODALS -->
        <div id="array_sample_cars" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="arrCarsResult">Result</h4>
                     <br>
                    <code class="codspace">
                        var cars = new Array("Saab", "Volvo", "BMW");
                        document.getElementById("demo").innerHTML = cars;
                    </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <h3>JavaScript Arrays</h3>
                    <p>Saab,Volvo,BMW</p>
                </div>
            </div>
        </div>         
        <div id="object_sample_person" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="objPersonResult">Result</h4>
                     <br>
                       <code class="codespace">
                        // Create an object:
                        var person = {
                            firstName : "John",
                            lastName  : "Doe",
                            age       : 50,
                            eyeColor  : "blue"
                        };

                        // Display some data from the object:
                        document.getElementById("demo").innerHTML =
                        person.firstName + " is " + person.age + " years old.";
                       </code>
                  </div>
                    <p><strong>Output:</strong></p>
                    <h3>JavaScript Objects</h3>
                    <p>John is 50 years old.</p>
                </div>
            </div>
        </div> 
</asp:Content>

<asp:Content ID="webProgLinks" ContentPlaceHolderID="sectionLinks" runat="server">
    <h2>Useful Links <span class="glyphicon glyphicon-link"></span></h2>
        <ul>
        <li><a href="https://www.w3schools.com/js/js_arrays.asp">W3Schools Arrays</a></li>
        <li><a href="https://www.w3schools.com/js/js_objects.asp">W3Schools Objects</a></li>
    </ul>
</asp:Content>