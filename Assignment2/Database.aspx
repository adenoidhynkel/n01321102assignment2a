﻿<%@ Page Title="Database Design & Development" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Assignment2.Database" %>
<asp:Content ID="dBMain" ContentPlaceHolderID="MainContent" runat="server">
     <div class="jumbotron">
        <h1><%: Page.Title %></h1>
    </div>
     <h2>Group By  <span class="glyphicon glyphicon-list-alt"></span></h2>
     <h3>Brief Description:</h3> 
     <p> A <em class="text-info">GROUP BY</em> clause allows a user to use aggregate functions such as <em class="text-info">MIN()</em>, <em class="text-info">MAX()</em>, <em class="text-info">ROUND()</em>, <em class="text-info">AVG()</em>, and <em class="text-info">SUM()</em>.
     </p>
     <h3>Why is it tricky?</h3>
      <p>
          I found it tricky and confusing at first because I had no idea how to use it. I tend to use it before the <em class="text-info">WHERE</em> clause and it always throws me an error. I had to learn the proper format when using the function
          and I need to pay attention on the problem or question given to do it properly. </p>
</asp:Content>

<asp:Content ID="dBCodeSnippet" ContentPlaceHolderID="sectionCode" runat="server">
       <h2>Code Examples <span class="glyphicon glyphicon-chevron-left"></span><span class="glyphicon glyphicon-chevron-right"></span></h2>
       <p class="whitespace">Following the topic above, here are some examples:</p>
       <ol id="codeExamples">    
        <li>If the question requires us to find the total salary of each employee working in a factory; We would get the total salary and will group by workplace.
        <div class="col-md-12 col-xs-12 well well-sm">
           <code>
            SELECT workplace, SUM(employee_salary) AS "Total Salary"
            FROM employees
            GROUP BY workplace;
           </code>
         </div>
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#query_example_groupby">Show Result</button>
        </li>
        <li> We can also find the total salary of each employee working in a factory; We would get the total salary and filter the working place with only factory.
        <div class="col-md-12 col-xs-12 well well-sm">
           <code>
            SELECT workplace, SUM(employee_salary) AS "Total Salary"
            FROM employees
            GROUP BY workplace
            HAVING UPPER(workplace) = 'FACTORY';
           </code>
        </div>
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#query_example_having">Show Result</button>
        </li>
        </ol>

    <!-- MODALS -->
        <div id="query_example_groupby" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="groupByResult">Result</h4>
                     <br>
                    <code>
                        SELECT workplace, SUM(employee_salary) AS "Total Salary"
                        FROM employees
                        GROUP BY workplace;
                    </code>
                  </div>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                            <th>Workplace</th>
                            <th>Total Salary</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Restaurant</td>
                                <td>30000.00</td>
                            </tr>                            
                            <tr>
                                <td>Factory</td>
                                <td>27000.00</td>
                            </tr>                            
                            <tr>
                                <td>Hospital</td>
                                <td>90000.00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 

         <div id="query_example_having" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                 <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="groupByHavingResult">Result</h4>
                     <br>
                    <code>
                        SELECT workplace, SUM(employee_salary) AS "Total Salary"
                        FROM employees
                        GROUP BY workplace
                        HAVING UPPER(workplace) = 'FACTORY';
                    </code>
                  </div>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                            <th>Workplace</th>
                            <th>Total Salary</th>
                            </tr>
                        </thead>
                        <tbody>                          
                            <tr>
                                <td>Factory</td>
                                <td>27000.00</td>
                            </tr>                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</asp:Content>

<asp:Content ID="dBLectureContent" ContentPlaceHolderID="sectionLecture" runat="server">
    <h2>More Examples <span class="glyphicon glyphicon-chevron-left"></span><span class="glyphicon glyphicon-chevron-right"></span></h2>
    <ol id="moreExamples">
    <li>The general syntax is:
    <div class="col-md-12 col-xs-12 well well-sm">
        <code class="whitespace">
            SELECT column-names
            FROM table-name
            WHERE condition
            GROUP BY column-names
            HAVING condition

        </code>
    </div>    
    </li>    
    <li>The general syntax with <strong class="text-info">ORDER BY</strong> is:
    <div class="col-md-12 col-xs-12 well well-sm">
        <code class="whitespace">
            SELECT column-names
            FROM table-name
            WHERE condition
            GROUP BY column-names
            HAVING condition
            ORDER BY column-names

        </code>
    </div>    
    </li>
    <li>
    <strong>Problem:</strong> List the number of customers in each country. Only include countries with more than 10 customers.
    <div class="col-md-12 col-xs-12 well well-sm">
        <code>
            SELECT COUNT(Id), Country FROM Customer GROUP BY Country HAVING COUNT(Id) > 10
        </code>
    </div>
    <br/>
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#query_example_list_cust">Show Result</button>
    </li>
    <li>
    <strong>Problem:</strong> List the number of customers in each country, except the USA, sorted high to low. Only include countries with 9 or more customers.
    <div class="col-md-12 col-xs-12 well well-sm">
        <code>
            SELECT COUNT(Id), Country FROM Customer WHERE Country <> 'USA' GROUP BY Country HAVING COUNT(Id) >= 9 ORDER BY COUNT(Id) DESC
        </code>
    </div>
    <br/>
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#query_example_sort">Show Result</button>
    </li>    
    <li>
    <strong>Problem:</strong> List all customer with average orders between $1000 and $1200.
    <div class="col-md-12 col-xs-12 well well-sm">
        <code class="whitespace">
           SELECT AVG(TotalAmount), FirstName, 
           LastName FROM Order O JOIN Customer C ON O.CustomerId = C.Id
           GROUP BY FirstName, LastName
           HAVING AVG(TotalAmount) BETWEEN 1000 AND 1200

        </code>
    </div>
    <br/>
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#query_example_average">Show Result</button>
    </li>
    </ol>

    <!-- MODALS -->
        <div id="query_example_list_cust" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="listCustResult">Result</h4>
                     <br>
                    <code>
                        SELECT COUNT(Id), Country FROM Customer GROUP BY Country HAVING COUNT(Id) > 10
                    </code>
                  </div>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                            <th>Count</th>
                            <th>Country</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>11</td>
                                <td>France</td>
                            </tr>                            
                            <tr>
                                <td>11</td>
                                <td>Germany</td>
                            </tr>                            
                            <tr>
                                <td>13</td>
                                <td>USA</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 

         <div id="query_example_sort" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                 <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="sortResult">Result</h4>
                     <br>
                    <code>
                        SELECT COUNT(Id), Country FROM Customer WHERE Country <> 'USA' GROUP BY Country HAVING COUNT(Id) >= 9 ORDER BY COUNT(Id) DESC
                    </code>
                  </div>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                            <th>Count</th>
                            <th>Country</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>11</td>
                                <td>France</td>
                            </tr>                            
                            <tr>
                                <td>11</td>
                                <td>Germany</td>
                            </tr>                            
                            <tr>
                                <td>9</td>
                                <td>Brazil</td>
                            </tr>                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>         
        
        <div id="query_example_average" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                 <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="averageResult">Result</h4>
                     <br>
                    <code class="whitespace">
                    SELECT AVG(TotalAmount), FirstName, LastName
                    FROM [Order] O JOIN Customer C ON O.CustomerId = C.Id
                    GROUP BY FirstName, LastName
                    HAVING AVG(TotalAmount) BETWEEN 1000 AND 1200

                    </code>
                  </div>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                            <th>Average</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1081.215000</td>
                                <td>Miguel</td>
                                <td>Angel Paolino</td>
                            </tr>                            
                            <tr>
                                <td>1063.420000</td>
                                <td>Isabel</td>
                                <td>de Castro</td>
                            </tr>                            
                            <tr>
                                <td>1008.440000</td>
                                <td>Alexander</td>
                                <td>Feuer</td>
                            </tr>                                 
                            <tr>
                                <td>1062.038461</td>
                                <td>Thomas</td>
                                <td>Hardy</td>
                            </tr>                             
                            <tr>
                                <td>1107.806666</td>
                                <td>Pirkko</td>
                                <td>Koskitalo</td>
                            </tr>                             
                            <tr>
                                <td>1174.945454</td>
                                <td>Janete</td>
                                <td>Limeira</td>
                            </tr>                             
                            <tr>
                                <td>1073.621428</td>
                                <td>Antonio</td>
                                <td>Moreno</td>
                            </tr>                             
                            <tr>
                                <td>1065.385000</td>
                                <td>Rita</td>
                                <td>Müller</td>
                            </tr>                                                        
                            <tr>
                                <td>1183.010000</td>
                                <td>José</td>
                                <td>Pedro Freyre</td>
                            </tr>   
                            <tr>
                                <td>1057.386666</td>
                                <td>Carine</td>
                                <td>Schmitt</td>
                            </tr>  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</asp:Content>

<asp:Content ID="dBLinks" ContentPlaceHolderID="sectionLinks" runat="server">
    <h2>Useful Links <span class="glyphicon glyphicon-link"></span></h2>
    <ul>
        <li><a href="https://www.w3schools.com/sql/sql_groupby.asp">W3Schools</a></li>
        <li><a href="https://www.dofactory.com/sql/having">Dofactory</a></li>
        <li><a href="https://www.guru99.com/group-by.html">Guru 99</a></li>
        <li><a href="https://www.techonthenet.com/sql/group_by.php">Techonthenet</a></li>
    </ul>
</asp:Content>
