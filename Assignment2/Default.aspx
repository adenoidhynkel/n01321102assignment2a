﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Review Materials</h1>
        <p class="lead">Materials include Database Design & Development, Web Application Development, and Web Programming</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Database Design & Development</h2>
            <p>
                This Review Material will talk about the <span class="text-info">GROUP BY</span> Clause. 
                It will provide a brief definition, the author's interpretation and more.
            </p>
            <p>
                <a class="btn btn-primary" href="Database.aspx">Check it out &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Application Development</h2>
            <p>
             The Review Material will talk about the Conditional statments and loops. 
             It will provide a brief definition, the author's interpretation and more.
            </p>
            <p>
                <a class="btn btn-primary" href="WebAppDev.aspx">Check it out &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web <br>Programming</h2>
            <p>
             This Review Material will talk about the objects and arrays. 
             It will provide a brief definition, the author's interpretation and more.
            </p>
            <p>
                <a class="btn btn-primary" href="WebProgramming.aspx">Check it out &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
